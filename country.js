
onload=getdata;
for (var j=0; j<1; j++)
{
    getdata(j)
}

function getdata(j)
{
    url="https://restcountries.com/v2/all";
    fetch(url).then( (data) => {
        
        return data.json();
    })
    .then( (actualdata) => 
    {
           
        contain=document.getElementById('container')
        container=document.createElement('div')
        container.className='cardx'

        img=document.createElement("img")
        img.src=actualdata[j].flag;
        img.className="imgclass"
        container.appendChild(img)

        h2=document.createElement("h2")
        h2.innerText=actualdata[j].name;
        h2.className="h2"
        container.appendChild(h2)

        p1=document.createElement("p")
        p1.innerText="Currency: "+ actualdata[j].currencies.map( (ele) =>{return ele.name})
        container.appendChild(p1)

        p2=document.createElement("p")
        p2.innerText="Current Date and Time: "+ actualdata[j].timezones
        container.appendChild(p2)

        btn1=document.createElement("input")
        btn1.type="button"
        btn1.value="Show Map"
        btn1.className="btn1"
        a1=document.createElement("a")
        a1.target="_blank"
        a1.href="https://www.google.com/maps/place/"+ actualdata[j].name;
        a1.appendChild(btn1)
        container.appendChild(a1)

        btn2=document.createElement("input")
        btn2.type="button"
        btn2.value="Detail"
        btn2.className="btn2"
        a2=document.createElement("a")
        a2.target="_blank"
        a2.href="https://restcountries.com/v3.1/alpha/"+ actualdata[j].name;
        a2.appendChild(btn2)
        container.appendChild(a2)

        contain.appendChild(container)
        
    })
    .catch( (error) => {
        console.log(`Error: ${error}`)
    })
}

